package ro.ubb.bookstore.core.domain.validators;
import org.springframework.stereotype.Component;
import ro.ubb.bookstore.core.domain.Client;
@Component
public class ClientValidator  implements Validator<Client>{
    public void validate(Client entity) throws ValidatorException {
        if(entity.getFirst().isEmpty() || entity.getLast().isEmpty())
        throw new ValidatorException("[ERROR] 1st and last name required!");

        if(entity.getAddress().isEmpty())
            throw new ValidatorException("[ERROR] Invalid address!");
    }
}
