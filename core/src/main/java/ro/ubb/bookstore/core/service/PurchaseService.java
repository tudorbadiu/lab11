package ro.ubb.bookstore.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.bookstore.core.domain.Purchase;
import ro.ubb.bookstore.core.domain.validators.Validator;
import ro.ubb.bookstore.core.domain.validators.ValidatorException;
import ro.ubb.bookstore.core.JPARepo.JPAPurchaseRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class PurchaseService {
    public static final Logger log = LoggerFactory.getLogger(PurchaseService.class);
    @Autowired
    private JPAPurchaseRepository repo;
    @Autowired
    private Validator<Purchase> validator;
    public Optional<Purchase> addPurchase(Purchase pur) throws ValidatorException{
        log.trace("addPurchase-IN [purchase={}]",pur);
        validator.validate(pur);
        if(repo.existsById(pur.getId()))
        {log.trace("addPurchase-OUT [purchase={}]",pur);return Optional.of(pur);}
        repo.save(pur);
        log.debug("addPurchase-ADDED [purchase={}]",pur);
        log.trace("addPurchase-OUT [purchase={}]",pur);
        if(repo.existsById(pur.getId()))
            return Optional.empty();
        return Optional.of(pur);
    }
    public Set<Purchase> getAllPurchases(){
        log.trace("getAllPurchases-CALLED");return new HashSet<>(repo.findAll());
    }
    public Optional<Purchase> removePurchase(Long id) throws IllegalArgumentException{
        log.trace("removePurchase-IN [id={}]",id);
        Optional<Purchase> f=repo.findById(id);
        repo.deleteById(id);
        log.trace("removePurchase-OUT [id={}]",id);
        return f;
    }

}
