package ro.ubb.bookstore.core.JPARepo;

import ro.ubb.bookstore.core.domain.Purchase;

public interface JPAPurchaseRepository  extends BookstoreRepo<Purchase,Long> {
}
