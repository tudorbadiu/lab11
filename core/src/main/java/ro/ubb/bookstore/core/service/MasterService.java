package ro.ubb.bookstore.core.service;

import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.bookstore.core.domain.Book;
import ro.ubb.bookstore.core.domain.Client;
import ro.ubb.bookstore.core.domain.Purchase;
import ro.ubb.bookstore.core.domain.validators.ValidatorException;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
@Service
public class MasterService {
    public static final Logger log = LoggerFactory.getLogger(MasterService.class);
    @Autowired
    BookService book;
    @Autowired
    ClientService client;
    @Autowired
    PurchaseService purchase;
    /* client operations*/
    public Optional<Client> addClient(Client entity) throws ValidatorException { return client.addClient(entity);}
    public Set<Client> getAllClients(){return client.getAllClients();}
    public Optional<Client> removeClient(Long id) throws IllegalArgumentException {
        log.trace("removeClient-IN [id={}]",id);
        Optional<Client> ob=client.findOne(id);
        ob.ifPresent(value -> purchase.getAllPurchases().stream().filter(pur -> pur.getClient().equals(value.getId())).forEach(pur -> purchase.removePurchase(pur.getId())));
        ob = client.removeClient(id);
        log.trace("removeClient-OUT [id={}]",id);
        return ob;
    }
    public Optional<Client> update(Client entity) throws ValidatorException {return client.update(entity);}
    public Set<Client> findByAdress(String addr){return client.findByAdress(addr);}

    /* book operations*/
    public Optional<Book> addBook(Book entity) throws ValidatorException{return book.addBook(entity);}
    public Set<Book> getAllBooks(){return book.getAllBooks();}
    public Optional<Book> removeBook(Long id) throws IllegalArgumentException {
        log.trace("removeBook-IN [id={}]",id);
        Optional<Book> ob=book.findOne(id);
        ob.ifPresent(value -> purchase.getAllPurchases().stream().filter(pur -> pur.getBook().equals(value.getId())).forEach(pur -> purchase.removePurchase(pur.getId())));
        ob = book.removeBook(id);
        log.trace("removeBook-OUT [id={}]",id);
        return ob;
    }
    public Optional<Book> update(Book entity) throws ValidatorException {return book.update(entity);}
    public Set<Book> findByYear(int year){return book.findByYear(year);}
    public  Set<Book> findByName(String inName){return book.findByName(inName);}
    public Iterable<Book> sortTitle() throws ValidatorException{ log.trace("sortTitle-IN-OUT"); return book.sortTitle();}
    /* purchase operations*/
    public Optional<Purchase> addPurchase(Purchase pur) throws ValidatorException{
        log.trace("addPurchase-IN");
        if(client.findOne(pur.getClient()).isPresent()&&book.findOne(pur.getBook()).isPresent()){
            log.debug("addPurchase-ADDING");
            return purchase.addPurchase(pur);}
        throw new ValidatorException("Client or Book could not be found");
    }
    public Set<Purchase> getAllPurchases(){ return purchase.getAllPurchases();}
    public Optional<Purchase> removePurchase(Long id) throws IllegalArgumentException{ return purchase.removePurchase(id);}

    /*master operations*/
    public Set<String> formatPurchases(){ return purchase.getAllPurchases().stream().map(pur->
            client.findOne(pur.getClient()).get().getName()+" bought "+book.findOne(pur.getBook()).get().getTitle()+" on "+pur.getDate()).collect(Collectors.toSet());}
    public List<Pair<Client,Long>> spentMost(){
        return client.getAllClients().stream().map(
               client1->{Long spent=purchase.getAllPurchases().stream().filter(pur->pur.getClient()==client1.getId()).
                       map(pur->book.findOne(pur.getBook()).get().getPrice()).reduce(0L,(a, b)->a+b);
                    return new Pair<>(client1,spent);
               }
        ).sorted((a,b)->b.getValue().compareTo(a.getValue())).collect(Collectors.toList());
    }
    public List<Pair<Book,Integer>> mostBought(){
        return book.getAllBooks().stream().map(
                book1->{
                    Integer bought=Math.toIntExact(purchase.getAllPurchases().stream().filter(pur->pur.getBook()==book1.getId()).count());
                    return new Pair<>(book1,bought);
                }
        ).sorted((a,b)->b.getValue().compareTo(a.getValue())).collect(Collectors.toList());
    }
}
