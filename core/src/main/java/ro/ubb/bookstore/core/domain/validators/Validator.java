package ro.ubb.bookstore.core.domain.validators;

/***
 * Validator interface for validating Books and Clients.
 *
 * @param <T> The entity
 */
public interface Validator<T> {
    void validate(T entity) throws ValidatorException;
}
