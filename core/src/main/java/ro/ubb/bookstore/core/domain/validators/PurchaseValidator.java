package ro.ubb.bookstore.core.domain.validators;

import org.springframework.stereotype.Component;
import ro.ubb.bookstore.core.domain.Purchase;

import java.time.LocalDate;
@Component
public class PurchaseValidator implements Validator<Purchase>{
    @Override
    public void validate(Purchase entity) throws ValidatorException {
        try{
            LocalDate.parse(entity.getDate());
        }catch(Exception e){throw new ValidatorException("Invalid date format:"+entity.getDate());}
    }
}
