package ro.ubb.bookstore.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ro.ubb.bookstore.core.domain.Book;
import ro.ubb.bookstore.core.domain.validators.Validator;
import ro.ubb.bookstore.core.domain.validators.ValidatorException;
import ro.ubb.bookstore.core.JPARepo.JPABookRepository;

import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * Controller class for managing Book entities.
 *
 * @author Adi, Tudor
 *
 */
@Service
public class BookService {
    @Autowired
    private JPABookRepository repo;
    @Autowired
    private Validator<Book> validator;
    public static final Logger log = LoggerFactory.getLogger(BookService.class);
    /**
     * Constructor
     *
     * @param repo
     *            must be not null, {@link Repo}
     *
     */

    /**
     * Add the {@code book} to the {@code repo}.
     *
     * @param book
     *            must be not null, {@link Book}
     *
     * @return The {@link Book} that has been added.
     */
    public Optional<Book> addBook(Book book) throws ValidatorException{
        log.trace("addBook-IN [book={}]",book);
        validator.validate(book);
        if(repo.existsById(book.getId()))
        {log.trace("addBook-OUT [book={}]",book);return Optional.of(book);}
        log.debug("addBook-ADDING [book={}]",book);
        repo.save(book);
        log.trace("addBook-OUT [book={}]",book);
        if(repo.existsById(book.getId()))
            return Optional.empty();
        return Optional.of(book);
    }

    /**
     * Gets a certain book from {@code repo}
     * @param id The id of the {@link Book} to fetch
     *
     * @return Set
     */
    public Optional<Book> findOne(Long id){return repo.findById(id);}
    public Set<Book> getAllBooks(){
        log.trace("getAllBooks-called");
        return new HashSet<>(repo.findAll());
    }

    /**
     * Remove a book from the the {@code book repo}.
     *
     * @param id
     *            must be not null, {@link Book}
     *
     * @return The {@link Book} that has been removed.
     *
     */

    public Optional<Book> removeBook(Long id) throws IllegalArgumentException {
        log.trace("removeBook-IN [id={}]",id);
        Optional<Book> f=repo.findById(id);
        f.ifPresent(s->{log.debug("removeBook-REMOVING [id={}]",id);repo.deleteById(id);});
        log.trace("removeBook-OUT [id={}]",id);
        return f;
    }

    /**
     * Updates a book from the the {@code book repo}.
     *
     * @param book
     *            must be not null, {@link Book}
     *
     * @return The {@link Book} that has been updated, none if no book was updated
     */
    @Transactional
    public Optional<Book> update(Book book) throws ValidatorException {
        log.trace("updateBook-IN [book={}]",book);
        validator.validate(book);
        if(repo.findById(book.getId()).equals(book))
        {log.trace("updateBook-OUT [book={}]",book);return Optional.empty();}
        repo.findById(book.getId()).ifPresent(b->{b.setAuthor(book.getAuthor());b.setYear(book.getYear());b.setPrice(book.getPrice());b.setTitle(book.getTitle());log.debug("updateBook-UPDATED  [{}]",b);});
        log.trace("updateBook-OUT [book={}]",book);
        return Optional.of(book);
    }
    public Iterable<Book> sortTitle() throws ValidatorException{
        /*if(repo instanceof SortingRepository){
            SortingRepository<Long, Book> r=(SortingRepository<Long, Book>) repo;
            return r.findAll(new Sort("title"));
        }*/
        log.trace("sortTitle-CALLED");
        return repo.findAll(Sort.by(Sort.Direction.ASC,"title"));
    }
    public Set<Book> findByYear(int year){
        log.trace("findByYear-CALLED [year={}]",year);
        return StreamSupport.stream(repo.findAll().spliterator(),false).filter(x->x.getYear()==year).collect(Collectors.toSet());
    }
    public  Set<Book> findByName(String inName){
        log.trace("findByName-CALLED [string={}]",inName);
        return StreamSupport.stream(repo.findAll().spliterator(),false).filter(x->x.getTitle().toLowerCase().contains(inName.toLowerCase())).collect(Collectors.toSet());
    }
}
