package ro.ubb.bookstore.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.ubb.bookstore.core.domain.Client;
import ro.ubb.bookstore.core.domain.validators.Validator;
import ro.ubb.bookstore.core.domain.validators.ValidatorException;
import ro.ubb.bookstore.core.JPARepo.JPAClientRepository;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
@Service
public class ClientService {
    @Autowired
    private JPAClientRepository repo;
    @Autowired
    private Validator<Client> validator;
    public static final Logger log = LoggerFactory.getLogger(ClientService.class);

    /**
     * Add the {@code book} to the {@code repo}.
     *
     * @param client must be not null, {@link Client}
     *
     * @return The {@link Client} that was added.
     */
    public Optional<Client> addClient(Client client) throws ValidatorException {
        log.trace("addClient-IN [client={}]",client);
        validator.validate(client);
        if(repo.existsById(client.getId())){
            log.trace("addClient-OUT [client={}]",client);
            return Optional.of(client);}
        repo.save(client);
        log.debug("addClient-ADDED [client={}]",client);
        log.trace("addClient-OUT [client={}]",client);
        if(repo.existsById(client.getId()))
            return Optional.empty();
        return Optional.of(client);
    }
    public Optional<Client> findOne(Long id){return repo.findById(id);}
    /**
     * Gets all clients from {@code repo}
     *
     * @return Set
     */
    public Set<Client> getAllClients() {
        log.trace("getAllClients-CALLED");
        return new HashSet<>(repo.findAll());
    }
    public Optional<Client> removeClient(Long id) throws IllegalArgumentException {
        log.trace("removeClient-IN [id={}]",id);
        Optional<Client> f=repo.findById(id);
        repo.deleteById(id);
        log.trace("removeClient-OUT [id={}]",id);
        return f;
    }
    @Transactional
    public Optional<Client> update(Client client) throws ValidatorException {
        log.trace("updateClient-IN [client={}]",client);
        validator.validate(client);
        if(repo.findById(client.getId()).equals(client))
        {log.trace("updateClient-OUT [client={}]",client);return Optional.empty();}
        repo.findById(client.getId()).ifPresent(c->{c.setAddress(client.getAddress());c.setFirst(client.getFirst());c.setLast(client.getLast());log.trace("updateClient-UPDATED [client={}]",c);});
        log.trace("updateClient-OUT [client={}]",client);
        return Optional.of(client);
    }
    public Set<Client> findByAdress(String addr){
        log.trace("findByAdress-CALLED [addr={}]",addr);
        return StreamSupport.stream(repo.findAll().spliterator(),false).filter(x->x.getAddress().toLowerCase().contains(addr.toLowerCase())).collect(Collectors.toSet());
    }
}
