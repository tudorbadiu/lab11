package ro.ubb.bookstore.core.JPARepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ro.ubb.bookstore.core.domain.BaseEntity;

import java.io.Serializable;
@NoRepositoryBean
public interface BookstoreRepo<T extends BaseEntity<ID>,ID extends Serializable> extends JpaRepository<T,ID> {
}
