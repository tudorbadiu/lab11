package ro.ubb.bookstore.core.domain;


import lombok.*;

import javax.persistence.Entity;
import java.lang.reflect.Field;
import java.time.LocalDate;
@Entity
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Purchase extends BaseEntity<Long> {
    public static Purchase INVALID=new Purchase(-1);
    private String TransactionDate;
    private Long client;
    private Long book;
    public Purchase(){}
    public Purchase(int x){
        this.setId((long)-1);
    }
    public Purchase(Long client,Long book){
        TransactionDate=LocalDate.now().toString();
        this.client=client;
        this.book=book;
    }
    public Purchase(Long client,Long book,String dt){
        TransactionDate=dt;
        this.client=client;
        this.book=book;
    }
    public Purchase(LocalDate dt){
        TransactionDate=dt.toString();
    }
    public Long getClient(){
        return client;
    }
    public Long getBook(){
        return book;
    }
    public String getDate(){
        return TransactionDate;
    }
    @Override
    public String toString() {
        return "Purchase ID:"+super.getId()+"Client ID:" + client + "Book ID:" + book + " date:" + TransactionDate ;
    }

}
