package ro.ubb.bookstore.core.domain;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.lang.reflect.Field;

/***
 * Base class for {@link Book} and {@link Client}
 * @param <ID> The type of the id.
 */
@MappedSuperclass
public class BaseEntity<ID extends Serializable> implements Serializable {
    /***
     * the current id of the entity
     */
    @Id
    private ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                '}';
    }

}
