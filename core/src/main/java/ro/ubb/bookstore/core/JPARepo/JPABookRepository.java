package ro.ubb.bookstore.core.JPARepo;

import ro.ubb.bookstore.core.domain.Book;
public interface JPABookRepository extends BookstoreRepo<Book,Long> {
}
