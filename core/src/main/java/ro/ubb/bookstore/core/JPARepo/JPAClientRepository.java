package ro.ubb.bookstore.core.JPARepo;

import ro.ubb.bookstore.core.domain.Client;

public interface JPAClientRepository extends BookstoreRepo<Client,Long>  {
}
