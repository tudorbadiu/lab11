package ro.ubb.bookstore.core.domain;

import lombok.*;

import javax.persistence.Entity;

import java.util.Objects;

/**
 * Book class that holds information about a book.
 *
 * @author Adi, Tudor
 *
 */
@Entity
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Book extends BaseEntity<Long> {

    private String title, author;
    private int year; // Year of publication
    private long price;
    public static Book INVALID=new Book(-1);
    public Book(int x){
        this.setId((long)-1);
    }
    /**
     * Default constructor
     *
     */
    public Book()
    {
        this.title = "None";
        this.author = "None";
        this.price =0;
        this.year = 0;
    }

    /**
     * Constructor
     * @param title The title of the book
     * @param author The author of the book
     */
    public Book(String title, String author)
    {
        this.title = title;
        this.author = author;
        this.price=0;
        this.year = 0;
    }

    /**
     * Constructor
     * @param title The title of the book
     * @param author The author of the book
     * @param price The publisher of the book
     * @param year The year the book was published
     */
    public Book(String title, String author,long price, int year)
    {
        this.title = title;
        this.author = author;
        this.price = price;
        this.year = year;
    }

    @Override
    public String toString() {
        return "ID: "+ super.getId()+ '\'' +" - Book: " +
                "'" + title + '\'' +
                " written by '" + author + '\'' +
                ", price: '" + price + '\'' +
                " release year: " + year + '\n';
    }

    // ----- Getters -----

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public long getPrice() {
        return price;
    }

    public int getYear() {
        return year;
    }


    // ----- Setters -----

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(long price) {
        this.price=price;
    }

    public void setYear(int year) {
        this.year = year;
    }


    // Book comparator

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return year == book.year &&
                title.equals(book.title) &&
                author.equals(book.author);
    }



    @Override
    public int hashCode() {
        return Objects.hash(title, author, price, year);
    }
}


