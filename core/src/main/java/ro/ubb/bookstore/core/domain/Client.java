package ro.ubb.bookstore.core.domain;

import lombok.*;

import javax.persistence.Entity;
import java.util.Objects;

/**
 * Client class that holds information about a client.
 *
 * @author Adi, Tudor
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Client extends BaseEntity<Long> {
    private String firstName, lastName, address;
    public static Client INVALID=new Client(-1);
    public Client(int x){
        this.setId((long)-1);
    }

    /**
     * Default constructor
     *
     *
     */
    public Client()
    {
        this.firstName = "None";
        this.lastName = "None";
        this.address = "None";
    }

    /**
     * Constructor
     *
     * @param firstname The name of the client
     * @param lastname The name of the client
     * @param address The address of the client
     */
    public Client(String firstname,String lastname, String address)
    {
        this.firstName = firstname;
        this.lastName = lastname;
        this.address = address;
    }

    @Override
    public String toString() {
        return "ID: " + super.getId()+ '\''+" - Client: " +
                "'" + firstName +" "+lastName+ '\'' +
                ", Address: '" + address + '\'' + '\n';
    }

    // ---- Getters ----
    public String getName() {
        return firstName+" "+lastName;
    }
    public String getFirst() {
        return firstName;
    }
    public String getLast() {
        return lastName;
    }
    public String getAddress() {
        return address;
    }


    // ---- Setters ----

    public void setFirst(String name) {
        this.firstName = name;
    }
    public void setLast(String name) { this.lastName = name; }
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(firstName, client.firstName) && Objects.equals(lastName, client.lastName) &&
                Objects.equals(address, client.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName,lastName, address);
    }


}
