package ro.ubb.bookstore.core.domain.validators;

/***
 * Exception class for throwing {@link Validator} related exceptions.
 */
public class ValidatorException extends BookStoreException {
    public ValidatorException(String message) {
        super(message);
    }

    public ValidatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidatorException(Throwable cause) {
        super(cause);
    }
}
