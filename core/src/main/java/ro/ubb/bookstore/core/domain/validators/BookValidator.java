package ro.ubb.bookstore.core.domain.validators;

import org.springframework.stereotype.Component;
import ro.ubb.bookstore.core.domain.Book;

import java.util.Calendar;

/**
 * Class for validating Book input.
 *
 * @author Adi, Tudor
 *
 */
@Component
public class BookValidator implements Validator<Book> {

    /**
     * Class for validating the fields of the Book {@code Entity}
     *
     * @param entity
     *            must be not null, {@link ro.ubb.BookStore.domain.BaseEntity}
     * @throws ValidatorException
     *             if the fields of the Book are invalid (Year is greater than current year
     *             or Title and Author are empty Fields)
     */
    @Override
    public void validate(Book entity) throws ValidatorException {
        //update at a later date
        if(entity.getYear() > Calendar.getInstance().get(Calendar.YEAR))
            throw new ValidatorException("[ERROR] Invalid book year!");

        if(entity.getTitle().isEmpty() || entity.getAuthor().isEmpty())
        {
            throw new ValidatorException("[ERROR] The title and author of the book cannot be empty!");
        }
    }
}
