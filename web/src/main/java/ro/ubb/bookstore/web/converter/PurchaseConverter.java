package ro.ubb.bookstore.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.bookstore.core.domain.Book;
import ro.ubb.bookstore.core.domain.Purchase;
import ro.ubb.bookstore.web.dto.BookDto;
import ro.ubb.bookstore.web.dto.PurchaseDto;

@Component
public class PurchaseConverter extends BaseConverter<Purchase, PurchaseDto> {
    @Override
    public Purchase convertDtoToModel(PurchaseDto dto) {
        Purchase pur = Purchase.builder()
                .TransactionDate(dto.getTransactionDate())
                .client(dto.getClient())
                .book(dto.getBook())
                .build();
        pur.setId(dto.getId());
        return pur;
    }

    @Override
    public PurchaseDto convertModelToDto(Purchase pur) {
        PurchaseDto dto = PurchaseDto.builder()
                .TransactionDate(pur.getTransactionDate())
                .client(pur.getClient())
                .book(pur.getBook())
                .build();
        dto.setId(pur.getId());
        return dto;
    }
}