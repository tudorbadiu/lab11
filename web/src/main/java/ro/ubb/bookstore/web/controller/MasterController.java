package ro.ubb.bookstore.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.bookstore.core.domain.Book;
import ro.ubb.bookstore.core.domain.Client;
import ro.ubb.bookstore.core.domain.Purchase;
import ro.ubb.bookstore.core.service.MasterService;
import ro.ubb.bookstore.web.converter.BookConverter;
import ro.ubb.bookstore.web.converter.ClientConverter;
import ro.ubb.bookstore.web.converter.PurchaseConverter;
import ro.ubb.bookstore.web.dto.*;
import java.util.Optional;

@RestController
public class MasterController {
    public static final Logger log= LoggerFactory.getLogger(MasterController.class);

    @Autowired
    private MasterService service;

    @Autowired
    private ClientConverter clientConverter;
    @Autowired
    private PurchaseConverter purchaseConverter;
    @Autowired
    private BookConverter bookConverter;


    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    ClientsDto getClients() {
        log.trace("getClients-CALLED");
        return new ClientsDto(clientConverter
                .convertModelsToDtos(service.getAllClients()));

    }
    @RequestMapping(value = "/clients/address/{addr}", method = RequestMethod.GET)
    ClientsDto getByAddress(@PathVariable String addr) {
        log.trace("getByAddress{addr}-CALLED",addr);
        return new ClientsDto(clientConverter
                .convertModelsToDtos(service.findByAdress(addr)));

    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    ClientDto saveClient(@RequestBody ClientDto clientDto) {
        log.trace("saveClient[{c}]-IN",clientDto);
        Client res=service.addClient(clientConverter.convertDtoToModel(clientDto)).orElseGet(() -> Client.INVALID);
        log.trace("saveClient[{c}]-OUT",res);
        return clientConverter.convertModelToDto(res);
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    ClientDto updateClient(@PathVariable Long id,
                             @RequestBody ClientDto clientDto) {
        log.trace("updateClient[{c}]-IN",clientDto);
        Client res=service.update(clientConverter.convertDtoToModel(clientDto)).orElseGet(() -> Client.INVALID);
        log.trace("updateClient[{c}]-OUT",res);
        return clientConverter.convertModelToDto( res);
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    ClientDto deleteClient(@PathVariable Long id){
        log.trace("deleteClient[{id}]-IN",id);
        Client res=service.removeClient(id).orElseGet(() -> Client.INVALID);
        log.trace("deleteClient[{c}]-OUT",res);
        return clientConverter.convertModelToDto( res);
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    BooksDto getBooks() {
        log.trace("getBooks-CALLED");
        return new BooksDto(bookConverter
                .convertModelsToDtos(service.getAllBooks()));

    }
    @RequestMapping(value = "/books/year/{yr}", method = RequestMethod.GET)
    BooksDto getBooksYear(@PathVariable int yr) {
        log.trace("getBooks  year:"+yr+"-CALLED");
        return new BooksDto(bookConverter
                .convertModelsToDtos(service.findByYear(yr)));
    }
    @RequestMapping(value = "/books/name/{name}", method = RequestMethod.GET)
    BooksDto getBooksName(@PathVariable String name) {
        log.trace("getBooks name:"+name+"-CALLED");
        return new BooksDto(bookConverter
                .convertModelsToDtos(service.findByName(name)));
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    BookDto saveBook(@RequestBody BookDto bookDto) {
        log.trace("saveBook[{b}]-IN",bookDto);
        Book res=service.addBook(bookConverter.convertDtoToModel(bookDto)).orElseGet(() -> Book.INVALID);
        log.trace("saveBook[{b}]-OUT",res);
        return bookConverter.convertModelToDto(res);
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.PUT)
    BookDto updateBook(@PathVariable Long id,
                           @RequestBody BookDto bookDto) {
        log.trace("updateBook[{b}]-IN",bookDto);
        Book res=service.update(bookConverter.convertDtoToModel(bookDto)).orElseGet(() -> Book.INVALID);
        log.trace("updateBook[{b}]-OUT",res);
        return bookConverter.convertModelToDto( res);
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.DELETE)
    BookDto deleteBook(@PathVariable Long id){
        log.trace("deleteBook[{id}]-IN",id);
        Book res=service.removeBook(id).orElseGet(() -> Book.INVALID);
        log.trace("deleteBook[{b}]-OUT",res);
        return bookConverter.convertModelToDto( res);
    }
    @RequestMapping(value = "/purchases", method = RequestMethod.GET)
    PurchasesDto getPurchases() {
        log.trace("getPurchases-CALLED");
        return new PurchasesDto(purchaseConverter
                .convertModelsToDtos(service.getAllPurchases()));

    }

    @RequestMapping(value = "/purchases", method = RequestMethod.POST)
    PurchaseDto savePurchase(@RequestBody PurchaseDto purchaseDto) {
        log.trace("savePurchase[{b}]-IN",purchaseDto);
        Purchase res=service.addPurchase(purchaseConverter.convertDtoToModel(purchaseDto)).orElseGet(() -> Purchase.INVALID);
        log.trace("savePurchase[{b}]-OUT",res);
        return purchaseConverter.convertModelToDto(res);
    }

    @RequestMapping(value = "/purchases/{id}", method = RequestMethod.DELETE)
    PurchaseDto deletePurchase(@PathVariable Long id){
        log.trace("deletePurchase[{id}]-IN",id);
        Purchase res=service.removePurchase(id).orElseGet(() -> Purchase.INVALID);
        log.trace("deletePurchase[{b}]-OUT",res);
        return purchaseConverter.convertModelToDto(res);
    }
}
