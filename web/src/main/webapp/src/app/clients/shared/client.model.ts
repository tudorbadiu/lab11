export class Client {
  id: number;
  firstName: string;
  lastName: string;
  address: string;
}
