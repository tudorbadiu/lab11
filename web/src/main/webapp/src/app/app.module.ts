import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ClientsComponent} from "./clients/clients.component";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {ClientService} from "./clients/shared/client.service";

@NgModule({
  declarations: [
    AppComponent,ClientsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,

  ],
  providers: [ClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
